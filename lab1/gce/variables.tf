variable "machine_type" { default = "n1-standard-1" }
variable "zone" { default = "us-west1-a" }
variable "tags" { default = ["externalssh"] }
variable "image" { default = "debian-cloud/debian-8" }
variable "network" { default = "default" }
variable "user" { }
variable "source" { default = "./gce/scripts/" }
variable "destination" { default = "/tmp"}
variable "gce_ssh_user" { default = "ameerabbas" }
variable "gce_ssh_pub_key_file" { default = "~/.ssh/id_rsa.pub" }
variable "gce_ssh_pri_key_file" { default = "~/.ssh/id_rsa" }
